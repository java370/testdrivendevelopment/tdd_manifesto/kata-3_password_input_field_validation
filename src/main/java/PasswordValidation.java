import java.util.stream.Stream;

public class PasswordValidation {

    public boolean validator(String psswd) {

        if (psswd.length() < 8) {
            throw new IllegalArgumentException("Password must be at least 8 characters");
        } else {
            int digitCount = 0;
            int capitalLetter = 0;
            int smallLetter = 0;
            for (int i = 0; i < psswd.length(); i++) {
                if (psswd.charAt(i) >= '0' && psswd.charAt(i) <= '9') digitCount++;
                else if (psswd.charAt(i) >= 'A' && psswd.charAt(i) <= 'Z') capitalLetter++;
                else if (psswd.charAt(i) >= 'a' && psswd.charAt(i) <= 'z') smallLetter++;
            }
            if (digitCount < 2) {
                throw new IllegalArgumentException("The password must contain at least 2 numbers");
            } else if (capitalLetter < 1) {
                throw new IllegalArgumentException("Password must contain at least one capital letter");
            } else if (psswd.length() == (digitCount + capitalLetter + smallLetter)) {
                throw new IllegalArgumentException("Password must contain at least one special character");
            } else {
                return true;
            }
        }
    }
}
