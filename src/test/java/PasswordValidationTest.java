import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PasswordValidationTest {
    private PasswordValidation tdd;

    @BeforeEach
    void setUp() {
        tdd = new PasswordValidation();
    }

    @Test
    void shouldAtLeastEightDigit() {
        String psswd = "utjlkfj";
        var exception = assertThrows(IllegalArgumentException.class, () -> tdd.validator(psswd));
        assertEquals("Password must be at least 8 characters", exception.getMessage());
    }

    @Test
    void shouldContainAtLeastTwoNumbers() {
        String psswd = "kiolkmnhw";
        var exception = assertThrows(IllegalArgumentException.class, () -> tdd.validator(psswd));
        assertEquals("The password must contain at least 2 numbers", exception.getMessage());

    }

    @Test
    void shouldContainAtLeastOneCapitalLetter() {
        String psswd = "kmjud7aa9873";
        var exception = assertThrows(IllegalArgumentException.class, () -> tdd.validator(psswd));
        assertEquals("Password must contain at least one capital letter", exception.getMessage());
    }

    @Test
    void shouldContainAtLeastOneSpecialCharacter() {
        String psswd = "ch35jkolB";
        var exception = assertThrows(IllegalArgumentException.class, () -> tdd.validator(psswd));
        assertEquals("Password must contain at least one special character", exception.getMessage());
    }
}
